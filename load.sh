#!/bin/bash
# Find the machine with the lowest load.

USERNAME="user"
MACHINE_SUFFIX="machine.domain.com"
MACHINES=(machine1 machine2 machine3 machine4)
LOAD_AVG=()

echo "It is highly recommended to have your ssh key present on all machines you are going to check."
echo "If not, you may have to type in your password for each machine."
echo "Load is based on the average load over the last five minutes."

for ((i=0; i<${#MACHINES[@]}; i++)); do
    echo "checking ${MACHINES[i]}..."
    if ping -c1 ${MACHINES[i]}.${MACHINE_SUFFIX} >/dev/null 2>&1; then
        LOAD_AVG[i]=$(ssh -o "StrictHostKeyChecking no" ${USERNAME}@${MACHINES[i]}.${MACHINE_SUFFIX} \
            "cat /proc/loadavg" 2>/dev/null | sed -e 's/^\(\S*\s*\S*\s*\S*\).*$/\1/')
        echo "${LOAD_AVG[i]}"
        LOAD_AVG[i]=$(echo ${LOAD_AVG[i]} | sed -e 's/^\S*\s*\(\S*\)\s*.*$/\1/')
    else
        LOAD_AVG[i]=9999
        echo "no response"
    fi
done

lowest_name=${MACHINES[0]}
lowest_load=${LOAD_AVG[0]}
for ((i=0; i<${#MACHINES[@]}; i++)); do
    if [[ $(echo "${LOAD_AVG[i]} < ${lowest_load}" | bc) -eq 1 ]]; then
        lowest_name=${MACHINES[i]}
        lowest_load=${LOAD_AVG[i]}
    fi
done

echo
echo "Best Machine: ${lowest_name}"
echo "Load: ${lowest_load}"
