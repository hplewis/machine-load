# machine-load
Find the machine with the lowest average load over the past 5 minutes.

It is *highly* suggested to have an ssh key installed on each machine before doing this. If not, you will likely have to type in your password for each machine.
